DROP TABLE IF EXISTS orders;

CREATE TABLE orders (
  order_id INT auto_increment primary key,
  email varchar(50) not null,
  name varchar(50) not null,
  order_date date not null
);