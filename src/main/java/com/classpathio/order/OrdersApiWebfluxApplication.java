package com.classpathio.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersApiWebfluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersApiWebfluxApplication.class, args);
	}

}
