package com.classpathio.order.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtGrantedAuthoritiesConverterAdapter;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
@EnableWebFluxSecurity
public class WebSecurityConfiguration {
	
	@Bean
	//Non-Blocking
	public SecurityWebFilterChain configure(ServerHttpSecurity httpSecurity) throws Exception {
		
		httpSecurity.cors().disable();
		httpSecurity.csrf().disable();
		httpSecurity.headers().frameOptions().disable();
		
		httpSecurity.authorizeExchange()
			.pathMatchers("/login", "/logout", "/actuator/**")
				.permitAll()
			.pathMatchers(HttpMethod.GET, "/api/v1/orders/**")
			//	.hasAnyRole("Everyone", "admins", "super_admins")
				.permitAll()
			.pathMatchers(HttpMethod.POST, "/api/v1/orders**")
				.hasAnyRole("admins", "super_admins")
			.pathMatchers(HttpMethod.DELETE, "/api/v1/orders**")
				.hasAnyRole("super_admins")
			.anyExchange()
				.authenticated()
			.and()
			.oauth2ResourceServer()
			.jwt();
		return httpSecurity.build();
	}
	
	@Bean
	//doing the job of what jwt.io is doing
	public ReactiveJwtAuthenticationConverter jwtConverter() {
		ReactiveJwtAuthenticationConverter converter = new ReactiveJwtAuthenticationConverter();
		JwtGrantedAuthoritiesConverter grantedAuthorities = new JwtGrantedAuthoritiesConverter();
		grantedAuthorities.setAuthoritiesClaimName("groups");
		grantedAuthorities.setAuthorityPrefix("ROLE_");
		ReactiveJwtGrantedAuthoritiesConverterAdapter adaptor = new ReactiveJwtGrantedAuthoritiesConverterAdapter(grantedAuthorities);
		converter.setJwtGrantedAuthoritiesConverter(adaptor);
		return converter;
		
	}

}
