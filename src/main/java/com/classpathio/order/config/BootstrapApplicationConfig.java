package com.classpathio.order.config;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.stream.IntStream;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import com.classpathio.order.model.Order;
import com.classpathio.order.repository.OrderJpaRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class BootstrapApplicationConfig {
	
	private final OrderJpaRepository orderRepository;
	private final Faker faker = new Faker();
	
	//this method will be invoked by Spring boot once the application is ready
	@EventListener(ApplicationReadyEvent.class)
	public void onReady(ApplicationReadyEvent readyEvent) {	
		System.out.println("Application started::::");
		 IntStream.range(1, 10)
         .forEach(index -> {
             Order order = Order.builder()
            		                .email(faker.internet().emailAddress())
		                            .name(faker.name().firstName())
		                            .orderDate(LocalDate.now())
		                            .build();
           this.orderRepository.save(order).subscribe();
         });
	}

}
