package com.classpathio.order.exception;

import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ServerWebExchange;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RestControllerAdvice
@RequiredArgsConstructor
@Configuration
public class GlobalExceptionHandler implements ErrorWebExceptionHandler {
	
	private final ObjectMapper objectMapper;

	@Override
	public Mono<Void> handle(ServerWebExchange exchange, Throwable exception) {
		
		DataBufferFactory bufferFactory = exchange.getResponse().bufferFactory();
		exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
		exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
		String message = exception.getMessage();
		Error error = new Error(100, message);
		DataBuffer dataBuffer  = null;
		try {
			dataBuffer = bufferFactory.wrap(objectMapper.writeValueAsBytes(error));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return exchange.getResponse().writeWith(Mono.just(dataBuffer));
	}
}

@RequiredArgsConstructor
@Getter
class Error{
	private final int code;
	private final String message;
}
