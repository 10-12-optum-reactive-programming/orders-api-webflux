package com.classpathio.order.service;

import java.time.LocalDateTime;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.classpathio.order.event.OrderEvent;
import com.classpathio.order.event.OrderStatus;
import com.classpathio.order.model.Order;
import com.classpathio.order.repository.OrderJpaRepository;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class OrderService {
	private final OrderJpaRepository orderRepository;
	private final WebClient webClient;
	private final StreamBridge streamBridge;
	
	/*
	 * public OrderService(OrderJpaRepository orderRepository) {
	 * this.orderRepository = orderRepository; }
	 */
	
	
	public Mono<Order> saveOrder(Order order) {
		//make a rest call to the inventory microservice
		
		//push the event to the topic
		/*
		 * return this.webClient .post() .uri("/api/inventory") .exchangeToMono(response
		 * -> response.bodyToMono(Integer.class))
		 * .then(this.orderRepository.save(order));
		 * 
		 */
		this.orderRepository.save(order)
			.map(o -> new OrderEvent(order, OrderStatus.ORDER_ACCEPTED, LocalDateTime.now()))
			.map(orderEvent -> MessageBuilder.withPayload(orderEvent).build())
			.subscribe(event -> streamBridge.send("producer-out-0", event));
			
			
				
		/*Mono<OrderEvent> orderAcceptedEvent = Mono.just(new OrderEvent(savedOrder, OrderStatus.ORDER_ACCEPTED, LocalDateTime.now()));
		Message<Mono<OrderEvent>> orderEvent = MessageBuilder.withPayload(orderAcceptedEvent).build();*/
		
		//streamBridge.send("producer-out-0", orderEvent);
		return Mono.just(order);
	}
	
	
	public Flux<Order> fetchAllOrders() {
		return this.orderRepository.findAll();
	}
	
	public Mono<Order>fetchOrderById(long id) {
		/*
		 * Mono<Order> optionalOrder = this.orderRepository.findById(id); Mono<Order>
		 * monoOrder = optionalOrder.switchIfEmpty(Mono.error(new
		 * IllegalArgumentException("invalid order id passed")));
		 * return monoOrder;
		 * 
		 */
		return this.orderRepository.findById(id)
					.switchIfEmpty(Mono.error(new IllegalArgumentException("invalid order id passed")));
	}
	
	public Mono<Void> deleteOrderById(long id) {
		Mono<Order> order = this.fetchOrderById(id);
		/* optionalOrder.map(order -> this.orderRepository.delete(order)); */
		order.map(this.orderRepository::delete);
		 return Mono.empty();
	}
	
	public Mono<Order> deleteOrderByIdAndReturn(long id) {
		Mono<Order> order = this.fetchOrderById(id);
		/* optionalOrder.map(order -> this.orderRepository.delete(order)); */
		return order.flatMap(o -> this.orderRepository.delete(o).then(order));
	}
	
	
	public Mono<Order> updateOrder(long orderId, Order updatedOrder) {
		/*
		 * Mono<Order> monoOrder = this.orderRepository.findById(orderId); Mono<Order>
		 * updatedMonoOrder = monoOrder.map(dbRecord -> updatedOrder); Mono<Order> order
		 * = updatedMonoOrder.flatMap(orderToBeSaved ->
		 * this.orderRepository.save(orderToBeSaved)); return order;
		 */
		
		return this.fetchOrderById(orderId)
						.map(dbRecord -> updatedOrder)
						.flatMap(this.orderRepository::save);
	}
}
