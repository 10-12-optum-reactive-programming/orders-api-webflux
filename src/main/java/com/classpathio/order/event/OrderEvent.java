package com.classpathio.order.event;

import java.time.LocalDateTime;

import com.classpathio.order.model.Order;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Getter
public class OrderEvent {
	
	private final Order order;
	private final OrderStatus status;
	private final LocalDateTime timestamp;
	

}
