package com.classpathio.order.event;

public enum OrderStatus {
	ORDER_ACCEPTED,
	ORDER_REJECTED,
	ORDER_FULFILLED
}
