package com.classpathio.order.model;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Table("orders")
public class Order {
	
	@Id
	private long orderId;
	
	@NotEmpty(message="email address cannot be empty")
	@Email(message="email address is not in correct format")
	private String email;
	
	@NotEmpty(message="name cannot be empty")
	private String name;
	
	@NotNull(message="order date cannot be null")
	private LocalDate orderDate;

}
