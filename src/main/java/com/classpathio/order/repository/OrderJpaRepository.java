package com.classpathio.order.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.classpathio.order.model.Order;

@Repository
public interface OrderJpaRepository extends ReactiveCrudRepository<Order, Long>{

}
