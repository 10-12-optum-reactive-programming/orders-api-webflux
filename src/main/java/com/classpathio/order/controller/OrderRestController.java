package com.classpathio.order.controller;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpathio.order.model.Order;
import com.classpathio.order.service.OrderService;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {
	
	private final OrderService orderService;
	
	@GetMapping//(produces = {MediaType.TEXT_EVENT_STREAM_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Flux<Order> fetchAllOrder(){
		//return this.orderService.fetchAllOrders().delayElements(Duration.of(1, ChronoUnit.SECONDS));
		return this.orderService.fetchAllOrders();
	}
	
	@GetMapping("/{id}")
	public Mono<Order> fetchOrderById(@PathVariable("id") long id){
		return this.orderService.fetchOrderById(id);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Mono<Order> saveOrder(@RequestBody @Valid Order order){
		return this.orderService.saveOrder(order);
	}
	
	@PutMapping("/{id}")
	public Mono<Order> updateOrder(@PathVariable("id") long orderId, @RequestBody Order order){
		return this.orderService.updateOrder(orderId, order);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public Mono<Void> deleteOrderById(@PathVariable("id") long id){
		return this.orderService.deleteOrderById(id);
	}

}
